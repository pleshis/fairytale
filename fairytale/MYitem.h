//
//  MYitem.h
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYitem : NSObject

@property(nonatomic) int type;
@property(nonatomic,strong) UIImageView *image;

@end


#define OBSTACLE		0
#define ENEMY			1
#define POWER_UP		2
#define POWER_UP_LIFE	3
#define POWER_UP_HORSE	4
#define TYPES_OF_ITEM	5	//count items types
#define PRINCESS		6

