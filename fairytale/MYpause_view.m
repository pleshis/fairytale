//
//  MYpause_view.m
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYpause_view.h"
#import "MYgame_controller.h"
#import <QuartzCore/QuartzCore.h>

@implementation MYpause_view

- (id)init_with_MYgame_controller:(MYgame_controller*)game_controller{
	
	self = [[[NSBundle mainBundle] loadNibNamed:@"MYpause_view" owner:game_controller options:nil] objectAtIndex:0];
	
    if (!self) {
		return self;
    }
	
	[[self layer] setCornerRadius:50.0];
	
    return self;
}

- (void)awakeFromNib {
	[self start_animating];
}

-(void)start_animating{
	
	NSLog(@"test");
	
	[_sleepy setAnimationDuration:1.0/2];
	[_sleepy setAnimationImages:[NSArray arrayWithObjects:
								 [UIImage imageNamed:@"sleep_01"],
								 [UIImage imageNamed:@"sleep_02"],nil]];
	[_sleepy startAnimating];
}


@end
