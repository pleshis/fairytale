//
//  MYcredits_view.h
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MYmenu_controller;

@interface MYcredits_view : UIView

- (id)init_with_MYmenu_controller:(MYmenu_controller*)menu_controller;

@end
