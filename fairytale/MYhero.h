//
//  MYhero.h
//  fairytale
//
//  Created by Jan Plesek on 20.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MYitem;
@class MYgame_controller;

@interface MYhero : NSObject


@property(nonatomic) int in_action;	//finite machine
@property(nonatomic,strong) UIImageView *image;
@property(nonatomic,strong) NSArray *run_array;
@property(nonatomic,strong) NSArray *run_sword_array;
@property(nonatomic,strong) NSArray *horse_array;
@property(nonatomic) int lives;
@property(nonatomic) BOOL at_horse;
@property(nonatomic) CGPoint ground_position;
@property(nonatomic,strong) MYgame_controller *game_controller;



-(id)initWithPosition:(CGPoint)position game_controller:(MYgame_controller*)game_controller;
-(BOOL)has_collision:(MYitem*)item;
-(void)jump:(CGSize)screen_resolution;
-(void)fight;
-(void)pause;
-(void)play;

@end


#define HERO_WIDTH			94
#define HERO_HEIGHT			112
#define HERO_SWORD_WIDTH	130
#define SWORD_WIDTH			67
#define SWORD_HEIGHT		88
#define HALF_JUMP_TIME		0.30f
#define HORSE_HALF_JUMP_TIME	1.30f
#define HERO_JUMP_HEIGHT	0.4f		//relatively
#define FIGHT_DURATION		0.6f
#define RESTORING_DURATION	0.8f
#define HERO_X_POSITION		0.3f
#define HORSE_DURATION		5.0f
#define HORSE_WIDTH			145
#define HORSE_HEIGHT		171

#define EARTH_POSITION	0.8

#define ACTION_NORMAL		0
#define ACTION_JUMPING_UP	1
#define ACTION_JUMPING_DOWN	2
#define ACTION_FIGHTING		3
#define ACTION_RESTORING	4
#define ACTION_RESTORING_JUMPING_UP		5
#define ACTION_RESTORING_JUMPING_DOWN	6


#define STARTING_LIVES	3

