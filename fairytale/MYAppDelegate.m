//
//  MYAppDelegate.m
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYAppDelegate.h"
#import "MYgame_controller.h"
#import "MYmenu_controller.h"

@implementation MYAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
	
//	[self start_menu];
	
	_menu_controller = [[MYmenu_controller alloc] init];
	self.window.rootViewController = _menu_controller;
	
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)
-(void)setNewViewToLandscape:(UIView*)viewObject {
	//assumes self.view is landscape and viewObject is portrait
	[viewObject setCenter:CGPointMake( viewObject.frame.size.height/2,viewObject.frame.size.width/2)];
	CGAffineTransform cgCTM = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90));
	viewObject.transform = cgCTM;
	viewObject.bounds = CGRectMake(0, 0, viewObject.bounds.size.width, viewObject.bounds.size.height);
}


-(void)start_new_game{
	
	
	_game_controller = [[MYgame_controller alloc] init];
	
	[self setNewViewToLandscape:_game_controller.view];
	[UIView transitionFromView:_menu_controller.view
						toView:_game_controller.view
					  duration:ANIMATION_TIME
					   options:UIViewAnimationOptionTransitionCurlUp
					completion:^(BOOL finished) {
						
					}];
	
	
	
//	_game_controller.modalTransitionStyle = UIModalTransitionStylePartialCurl;
//	[_menu_controller presentViewController:_game_controller animated:YES completion:nil];
	
//	self.window.rootViewController = game_controller;
}

-(void)start_menu{
	
	_menu_controller = [[MYmenu_controller alloc] init];
	
	[self setNewViewToLandscape:_menu_controller.view];
	[UIView transitionFromView:_game_controller.view
						toView:_menu_controller.view
					  duration:ANIMATION_TIME
					   options:UIViewAnimationOptionTransitionCurlDown
					completion:^(BOOL finished) {
						
					}];
	
//	_menu_controller.modalTransitionStyle = UIModalTransitionStylePartialCurl;
//	[_game_controller presentViewController:_menu_controller animated:YES completion:nil];
//	self.window.rootViewController = menu_controller;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
