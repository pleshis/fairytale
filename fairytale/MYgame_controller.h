//
//  MYgame_controller.h
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <UIKit/UIKit.h>



@class MYhero;
@class MYpause_view;
@class MYvictory_view;
@class MYdead_view;


@interface MYgame_controller : UIViewController <UIAccelerometerDelegate>


//User Interface
@property (strong, nonatomic) IBOutlet UILabel *distance_label;

//objects
@property (strong, nonatomic) MYhero *hero;
@property (strong, nonatomic) UIImageView *earth, *earth2;
@property (strong, nonatomic) NSMutableArray *items_in_map;
@property (strong, nonatomic) NSMutableArray *show_lives;
@property (nonatomic) int distance;
@property (nonatomic) int score;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) BOOL pause;


@property (nonatomic) CGSize screen_resolution;


@property(nonatomic,strong) MYpause_view *pause_view;
@property(nonatomic,strong) MYvictory_view *victory_view;
@property(nonatomic,strong) MYdead_view *dead_view;

-(IBAction)restart_the_game:(id)sender;
-(IBAction)press_pause:(id)sender;
-(IBAction)take_me_to_game:(id)sender;

-(void)play_sound:(int)sound_id;

@end


#define FPS	60


#define WHEN_GENERATE_ITEM	80	//every x/FPS in seconds

#define OBSTACLE_WIDTH	135
#define OBSTACLE_HEIGHT	41

#define ENEMY_WIDTH		57
#define ENEMY_HEIGHT	107

#define POWER_UP_WIDTH	48
#define POWER_UP_HEIGHT	44

#define PRINCESS_WIDTH	91
#define PRINCESS_HEIGHT	107

#define EARTH_POSITION	0.8

#define ITEMS_VELOCITY	12
#define MAX_SCORE		50

#define SOUND_HORSE		0
#define SOUND_JUMP		1
#define SOUND_SWORD		2
#define SOUND_SCREAM	3
#define SOUND_FAILED	4
#define SOUND_VICTORY	5
#define SOUND_COIN		6

