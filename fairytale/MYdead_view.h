//
//  MYdead_view.h
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MYgame_controller;

@interface MYdead_view : UIView

- (id)init_with_MYgame_controller:(MYgame_controller*)game_controller;

@end
