//
//  MYRestaurant.h
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MYRestaurant : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) double price;
@property (nonatomic) double rating;
@property (nonatomic, retain) NSDate * last_modified;

-(void)add_one_star;

@end
