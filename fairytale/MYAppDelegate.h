//
//  MYAppDelegate.h
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MYgame_controller;
@class MYmenu_controller;

@interface MYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MYgame_controller *game_controller;
@property (strong, nonatomic) MYmenu_controller *menu_controller;

-(void)start_new_game;
-(void)start_menu;

@end


#define ANIMATION_TIME		0.4	//sec
#define ANIMATION_OFFSET	0.2	//move according to height

