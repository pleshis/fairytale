//
//  MYgame_controller.m
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "MYgame_controller.h"
#import "MYitem.h"
#import "MYhero.h"
#import "MYpause_view.h"
#import "MYvictory_view.h"
#import "MYdead_view.h"
#import <AudioToolbox/AudioToolbox.h>


@interface MYgame_controller ()

@end

@implementation MYgame_controller

- (id)init{
    self = [super initWithNibName:@"MYgame_controller" bundle:nil];
    if(!self) {
		return self;
    }
	
	//Here load everything
	[UIAccelerometer sharedAccelerometer].delegate = self;
	
	
	//screen resolution
	_screen_resolution = [[UIScreen mainScreen] bounds].size;
	_screen_resolution = CGSizeMake(MAX(_screen_resolution.width,_screen_resolution.height),
									MIN(_screen_resolution.width,_screen_resolution.height));
	
	
	
	
    return self;
}

-(void)play_sound:(int)sound_id{
    NSString *soundPath;
	
	switch (sound_id) {
		case SOUND_HORSE:
			soundPath = [[NSBundle mainBundle] pathForResource:@"horse" ofType:@"aiff"];
			break;
		case SOUND_JUMP:
			soundPath = [[NSBundle mainBundle] pathForResource:@"jump" ofType:@"aiff"];
			break;
		case SOUND_SWORD:
			soundPath = [[NSBundle mainBundle] pathForResource:@"sword" ofType:@"aiff"];
			break;
		case SOUND_SCREAM:
			soundPath = [[NSBundle mainBundle] pathForResource:@"scream" ofType:@"aiff"];
			break;
		case SOUND_FAILED:
			soundPath = [[NSBundle mainBundle] pathForResource:@"failed" ofType:@"aiff"];
			break;
		case SOUND_VICTORY:
			soundPath = [[NSBundle mainBundle] pathForResource:@"victory" ofType:@"aiff"];
			break;
		case SOUND_COIN:
			soundPath = [[NSBundle mainBundle] pathForResource:@"coin" ofType:@"aiff"];
			break;
			
		default:
			break;
	}
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
    AudioServicesPlaySystemSound (soundID);
}

-(IBAction)restart_the_game:(id)sender{
	
	
	
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_dead_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5+ANIMATION_OFFSET)*_screen_resolution.height)];
						 [_dead_view setAlpha:0.0];
                     }
					 completion:^(BOOL finished){
						 [_dead_view removeFromSuperview];
					 }];
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_victory_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5+ANIMATION_OFFSET)*_screen_resolution.height)];
						 [_victory_view setAlpha:0.0];
                     }
					 completion:^(BOOL finished){
						 [_victory_view removeFromSuperview];
					 }];
	
	
	if(_hero){
		[[_hero image] removeFromSuperview];
	}
	
	[_show_lives makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	
	for (MYitem *item in _items_in_map) {
		[[item image] removeFromSuperview];
	}
	
	
	
	//hero
	_hero = [[MYhero alloc] initWithPosition:CGPointMake(_screen_resolution.width *HERO_X_POSITION,
														 _screen_resolution.height*EARTH_POSITION+8)
							 game_controller:self];
	
	[self.view addSubview:_hero.image];
	
	
	
	_show_lives = [[NSMutableArray alloc] init];
	
	
	_pause = NO;
	[_hero play];
	
	[self update_lives];
	
	//items in the map
	
	_items_in_map = [[NSMutableArray alloc] init];
	
	_score = 0;
	
	//start run
	[self take_me_to_game:nil];
	
}

-(void)update_lives{

	[_show_lives makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[_show_lives removeAllObjects];
	
	for(int i=0; i<[_hero lives]; i++){
		[_show_lives addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"live"]]];
		[self.view addSubview:[_show_lives lastObject]];
		[[_show_lives lastObject] setCenter:CGPointMake(30+i*35, 30)];
	}
	
	if([_show_lives count]==0){
		[self dead];
	}
	
	
}


-(void)dealloc{
	
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	
	
	
	//earth
	_earth = [[UIImageView alloc] initWithFrame:CGRectMake(0,_screen_resolution.height*EARTH_POSITION,
														   _screen_resolution.width, 200)];
	[_earth setImage:[UIImage imageNamed:@"earth"]];
	[self.view addSubview:_earth];
	
	_earth2 = [[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,_screen_resolution.height*EARTH_POSITION,
														   _screen_resolution.width, 200)];
	[_earth2 setImage:[UIImage imageNamed:@"earth"]];
	[self.view addSubview:_earth2];
	
	
	_show_lives = nil;
	
	
	[self restart_the_game:nil];
	
	
	
	
	
	
	
}



//change the scene little bit
-(void)update:(NSTimer*)timer{
	
	
//	NSLog(@"%d",[_hero in_action]);
	
	_distance++;
	
	[_distance_label setText:[NSString stringWithFormat:@"×%d",_score]];

	
	//generating
	if(_distance%(WHEN_GENERATE_ITEM)==0){
		[self generate_something];
	}
	
	
	
	//moving with items
	
	NSMutableArray *items_to_get_out = [[NSMutableArray alloc] init];
	
	for(MYitem *item in _items_in_map) {
		[[item image] setCenter:CGPointMake([item image].center.x-ITEMS_VELOCITY, [item image].center.y)];
		
		//collisions
		
		
		if([_hero has_collision:item]){
			
			
			switch ([item type]) {
				case POWER_UP:{
					
					[[item image] removeFromSuperview];
					[items_to_get_out addObject:item];
					
					_score += 3;
					
					break;
				}
				case POWER_UP_LIFE:{
					
					[[item image] removeFromSuperview];
					[items_to_get_out addObject:item];
					
					[_hero setLives:[_hero lives]+1];
					
					break;
				}
				case POWER_UP_HORSE:{
					
					[[item image] removeFromSuperview];
					[items_to_get_out addObject:item];
					
					
					_score += 1;
					
					break;
				}
				case ENEMY:{
					
					if([_hero in_action]==ACTION_FIGHTING){
						[[item image] removeFromSuperview];
						[items_to_get_out addObject:item];
						
						_score += 5;
					}
					
					break;
				}
				case PRINCESS:{
					
					[self play_sound:SOUND_VICTORY];
					NSLog(@"Victory");
					[self victory];
					
					
					
					break;
				}
					
				default:
					break;
			}
			
		}
			
		
		
		//end of the view (objects are out of screen)
		if([[item image] center].x<(-[[item image] frame].size.width/2)){
			[[item image] removeFromSuperview];
			[items_to_get_out addObject:item];
		}
			
		
	}
	
	[_items_in_map removeObjectsInArray:items_to_get_out];

	
	//moving with ground
	
	[_earth setCenter:CGPointMake([_earth center].x-ITEMS_VELOCITY, [_earth center].y)];
	[_earth2 setCenter:CGPointMake([_earth2 center].x-ITEMS_VELOCITY, [_earth2 center].y)];
	
	if([_earth frame].origin.x+[_earth frame].size.width<=0){
		[_earth setCenter:CGPointMake([_earth2 center].x+[_earth2 frame].size.width, [_earth2 center].y)];
	}
	
	if([_earth2 frame].origin.x+[_earth2 frame].size.width<=0){
		[_earth2 setCenter:CGPointMake([_earth center].x+[_earth frame].size.width, [_earth center].y)];
	}
	
	
	if([_hero lives]!=[_show_lives count]){
		[self update_lives];
	}
	
}



// generating items
-(void)generate_something{
	
	int item_type = rand() % TYPES_OF_ITEM + POWER_UP + 1;	//some of items more often
	
	
	if(item_type>=(TYPES_OF_ITEM)){
		item_type -= TYPES_OF_ITEM;
	}
	
	
	
	if(_score>=MAX_SCORE){
		//generate princess
		
		item_type = PRINCESS;
	}
	
	
//	if(item_type==NONE){
//		return;
//	}
	
	
	
	
	
	//generate some item
	MYitem *item = [[MYitem alloc] init];
	
	switch (item_type) {
		case OBSTACLE:{
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-OBSTACLE_HEIGHT+20,
																		 OBSTACLE_WIDTH, OBSTACLE_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"obstacle"]];
			break;
		}
		case ENEMY:{
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-ENEMY_HEIGHT+6,
			 															 ENEMY_WIDTH, ENEMY_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"enemy"]];
			break;
		}
		case POWER_UP:{
			
			int how_much_up = rand() % 5;
			
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-POWER_UP_HEIGHT*(3+how_much_up),
																		 POWER_UP_WIDTH, POWER_UP_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"coin_regular"]];
			break;
		}
		case POWER_UP_LIFE:{
			
			int how_much_up = rand() % 5;
			
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-POWER_UP_HEIGHT*(3+how_much_up),
																		 POWER_UP_WIDTH, POWER_UP_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"coin_life"]];
			break;
		}
		case POWER_UP_HORSE:{
			
			int how_much_up = rand() % 5;
			
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-POWER_UP_HEIGHT*(3+how_much_up),
																		 POWER_UP_WIDTH, POWER_UP_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"coin_horse"]];
			break;
		}
		case PRINCESS:{
			
			[item setImage:[[UIImageView alloc] initWithFrame:CGRectMake(_screen_resolution.width,
																		 _screen_resolution.height*EARTH_POSITION-PRINCESS_HEIGHT,
																		 PRINCESS_WIDTH, PRINCESS_HEIGHT)]];
			[[item image] setImage:[UIImage imageNamed:@"princess_01"]];
			
			//animation of princess
			
			break;
		}
		default:
			break;
	}
	
	[item setType:item_type];
	[[self view] addSubview:[item image]];
	[_items_in_map addObject:item];

}



- (void)didReceiveMemoryWarning{
	
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Accelerometer Delegate

- (void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {

	return;
#warning - Here will be something to pretend jump when im jumping already
	
	
//	NSLog(@"%02.3f %02.3f %02.3f", acceleration.x, acceleration.y, acceleration.z);
	float vector_length = sqrtf(acceleration.x*acceleration.x+acceleration.y*acceleration.y+acceleration.z*acceleration.z);
	
	if(fabs(vector_length)>1.3){
		NSLog(@"length = %f",vector_length);
	}
	
	
	
//	if (self.lastAcceleration) {
//		if (!histeresisExcited && L0AccelerationIsShaking(self.lastAcceleration, acceleration, 0.7)) {
//			histeresisExcited = YES;
//			
//			/* SHAKE DETECTED. DO HERE WHAT YOU WANT. */
//			
//		} else if (histeresisExcited && !L0AccelerationIsShaking(self.lastAcceleration, acceleration, 0.2)) {
//			histeresisExcited = NO;
//		}
//	}
//
//	self.lastAcceleration = acceleration;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	
    NSSet *allTouches = [event allTouches];
    for (UITouch *touch in allTouches){
		
        CGPoint location = [touch locationInView:self.view];
		
		
		if(_score<MAX_SCORE){
			if(location.x<_screen_resolution.width/2){
				[_hero jump:_screen_resolution];
			}
			else{
				[_hero fight];
			}
		}
    }
	
}


-(IBAction)press_pause:(id)sender{
	
	if(_pause){
		return;
	}
	
	_pause = YES;
	_pause_view = [[MYpause_view alloc] init_with_MYgame_controller:self];
	[_pause_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5-ANIMATION_OFFSET)*_screen_resolution.height)];
	[_pause_view setAlpha:0.0];
	[[self view] addSubview:_pause_view];
	
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_pause_view setCenter:CGPointMake(_screen_resolution.width/2, _screen_resolution.height/2)];
						 [_pause_view setAlpha:1.0];
                     }
					 completion:^(BOOL finished){
						 
					 }];
	
	
	[_timer invalidate];

	
	[_hero pause];
}


-(IBAction)take_me_to_game:(id)sender{
	
	
	_pause = NO;
	
	_timer = [NSTimer scheduledTimerWithTimeInterval:1.0/FPS
											  target:self
											selector:@selector(update:)
											userInfo:nil
											 repeats:YES];
	[_hero play];
	
	
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_pause_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5+ANIMATION_OFFSET)*_screen_resolution.height)];
						 [_pause_view setAlpha:0.0];
                     }
					 completion:^(BOOL finished){
						 [_pause_view removeFromSuperview];
					 }];
	
}

-(void)victory{
	
	
	_victory_view = [[MYvictory_view alloc] init_with_MYgame_controller:self];
	[_victory_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5-ANIMATION_OFFSET)*_screen_resolution.height)];
	[_victory_view setAlpha:0.0];
	[[self view] addSubview:_victory_view];
	
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_victory_view setCenter:CGPointMake(_screen_resolution.width/2, _screen_resolution.height/2)];
						 [_victory_view setAlpha:1.0];
                     }
					 completion:^(BOOL finished){
						 
					 }];
	
	
	[_timer invalidate];
	[_hero pause];
	
}



-(void)dead{
	
	
	[self play_sound:SOUND_FAILED];
	
	_dead_view = [[MYdead_view alloc] init_with_MYgame_controller:self];
	[_dead_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5-ANIMATION_OFFSET)*_screen_resolution.height)];
	[_dead_view setAlpha:0.0];
	[[self view] addSubview:_dead_view];
	
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_dead_view setCenter:CGPointMake(_screen_resolution.width/2, _screen_resolution.height/2)];
						 [_dead_view setAlpha:1.0];
                     }
					 completion:^(BOOL finished){
						 
					 }];
	
	
	[_timer invalidate];
	[_hero pause];
	
}

-(IBAction)take_me_to_menu:(id)sender{
	
	[(MYAppDelegate*)[[UIApplication sharedApplication] delegate] start_menu];
	[_timer invalidate];
}



@end
