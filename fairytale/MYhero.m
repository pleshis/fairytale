//
//  MYhero.m
//  fairytale
//
//  Created by Jan Plesek on 20.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MYhero.h"
#import "MYitem.h"
#import "MYgame_controller.h"

@implementation MYhero


-(id)initWithPosition:(CGPoint)position game_controller:(MYgame_controller*)game_controller{
	
	self = [super init];
	
	if(!self){
		return self;
	}
	
	
	_game_controller = game_controller;
	
	_run_array = [NSArray arrayWithObjects:
				  [UIImage imageNamed:@"hero_01"],
				  [UIImage imageNamed:@"hero_02"],
				  [UIImage imageNamed:@"hero_03"],nil];
	
	_run_sword_array = [NSArray arrayWithObjects:
						[UIImage imageNamed:@"hero_sword_01"],
						[UIImage imageNamed:@"hero_sword_02"],
						[UIImage imageNamed:@"hero_sword_03"],nil];
	
	_horse_array = [NSArray arrayWithObjects:
					[UIImage imageNamed:@"horse_02"],
					[UIImage imageNamed:@"horse_01"],
					[UIImage imageNamed:@"horse_02"],
					[UIImage imageNamed:@"horse_03"],nil];
	
	_ground_position = position;
		
	_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, HERO_WIDTH, HERO_HEIGHT)];
	[_image setCenter:CGPointMake(position.x, position.y-HERO_HEIGHT/2)];
	[_image setAnimationDuration:1.0/2];
	[_image setAnimationImages:_run_array];
	[_image startAnimating];
	
	
//	[_image setBackgroundColor:[UIColor redColor]];
//	[[_image layer] setBorderWidth:2];
//	[[_image layer] setBorderColor:[UIColor redColor].CGColor];
	
	_lives = STARTING_LIVES;
	
	_in_action = ACTION_NORMAL;
	
	return self;
}


-(BOOL)has_collision:(MYitem*)item{
	
	if(_in_action==ACTION_RESTORING){
		return NO;
	}
	
	
	if(CGRectIntersectsRect([[item image] frame], [[[_image layer] presentationLayer] frame])){
		
		//here should be something to detect just fist case of collision
		switch ([item type]) {
			case OBSTACLE:
				
				[_game_controller play_sound:SOUND_SCREAM];
				
				if(_at_horse){
					[self fall_down_from_horse];
				}
				else{
					
					switch (_in_action){
						case ACTION_NORMAL:{
							_in_action = ACTION_RESTORING;
							[self lost_life];
							
							[_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
							[_image setCenter:CGPointMake(_ground_position.x, _ground_position.y-HERO_HEIGHT/2)];
							[_image setAnimationImages:_run_array];
							[_image startAnimating];
							break;
						}
						case ACTION_JUMPING_UP:{
							[self lost_life];
							_in_action = ACTION_RESTORING_JUMPING_UP;
							break;
						}
						case ACTION_JUMPING_DOWN:{
							[self lost_life];
							_in_action = ACTION_RESTORING_JUMPING_DOWN;
							break;
						}
						default:
							break;
					}
					
					
					[_image setAlpha:0.4];
					[self performSelector:@selector(stop_restoring) withObject:nil afterDelay:RESTORING_DURATION];
				}
				
				break;
				
			case ENEMY:
				
				if(_in_action==ACTION_FIGHTING){
					//our hero hit enemy
					//increase score
				}
				else{
					
					[_game_controller play_sound:SOUND_SCREAM];
					if(_at_horse){
						[self fall_down_from_horse];
					}
					else{
					
						//enemy hit our hero
						[self lost_life];
						_in_action = ACTION_RESTORING;
						[_image setAlpha:0.6];
						[self performSelector:@selector(stop_restoring) withObject:nil afterDelay:RESTORING_DURATION];
					}
					
				}
				
				
				break;
				
			case POWER_UP:
				//					NSLog(@"Yeah!");
				//increase something
				
				[_game_controller play_sound:SOUND_COIN];
				break;
				
			case POWER_UP_LIFE:
				//					NSLog(@"Yeah!");
				//increase something
				
				[_game_controller play_sound:SOUND_COIN];
				break;
				
			case POWER_UP_HORSE:
				
				if(!_at_horse){
					[self take_a_horse];
				}
				
				break;
				
				
			default:
				break;
		}
		
		return YES;
	}
	
	return NO;
}




-(void)jump:(CGSize)screen_resolution{
	
	if((_in_action!=ACTION_NORMAL)&&(_in_action!=ACTION_RESTORING)&&(_in_action!=ACTION_FIGHTING)){
		return;
	}
	
	
	[_game_controller play_sound:SOUND_JUMP];
	
	if(_in_action==ACTION_RESTORING){
		_in_action = ACTION_RESTORING_JUMPING_UP;
	}
	else{	//ACTION_FIGHTING || ACTION_NORMAL
		_in_action = ACTION_JUMPING_UP;
	}
	
	if(!_at_horse){
		[_image stopAnimating];
		
		[_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
		[_image setCenter:CGPointMake(_ground_position.x, _ground_position.y-HERO_HEIGHT/2)];
		[_image setImage:[UIImage imageNamed:@"hero_jump"]];
	}
	
	
	[UIView animateWithDuration:(!_at_horse)?HALF_JUMP_TIME:HORSE_HALF_JUMP_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_image setCenter:CGPointMake(_image.center.x, _image.center.y-HERO_JUMP_HEIGHT*screen_resolution.height)];
                     }
					 completion:^(BOOL finished){
						 
						 
						 if(!_at_horse){
							 [_image stopAnimating];
						 }
							 
						 if(_in_action==ACTION_RESTORING_JUMPING_UP){
							 _in_action = ACTION_RESTORING_JUMPING_DOWN;
						 }
						 else if(_in_action==ACTION_JUMPING_UP){
							 _in_action = ACTION_JUMPING_DOWN;
						 }
						 
						 [UIView animateWithDuration:(!_at_horse)?HALF_JUMP_TIME:HORSE_HALF_JUMP_TIME
											   delay:0.0f
											 options:UIViewAnimationOptionCurveEaseIn
										  animations:^{
											  [_image setCenter:CGPointMake(_image.center.x, _image.center.y+HERO_JUMP_HEIGHT*screen_resolution.height)];
											  
										  }
										  completion:^(BOOL finished){
											  
											  if(_in_action==ACTION_JUMPING_DOWN){
												  _in_action = ACTION_NORMAL;
											  }
											  else{
												  _in_action = ACTION_RESTORING;
											  }
											  
											  if(!_at_horse){
												  [_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
												  [_image setCenter:CGPointMake(_ground_position.x, _ground_position.y-HERO_HEIGHT/2)];
												  [_image setAnimationImages:_run_array];
												  [_image startAnimating];
											  }
										  }];
					 }];
}


-(void)fight{
	
	if(_at_horse){	//not possible fight at horse
		return;
	}
	
	if(_in_action!=ACTION_NORMAL){
		return;
	}
	
	_in_action = ACTION_FIGHTING;
	
	
	[_game_controller play_sound:SOUND_SWORD];
	
	
	CGPoint center = [_image center];
	center.x += (HERO_SWORD_WIDTH-HERO_WIDTH)/2;
	[_image setFrame:CGRectMake(0,0,HERO_SWORD_WIDTH, HERO_HEIGHT)];
	[_image setCenter:CGPointMake(center.x, _ground_position.y-HERO_HEIGHT/2)];
	[_image setAnimationImages:_run_sword_array];
	[_image startAnimating];
	[self performSelector:@selector(stop_fight) withObject:nil afterDelay:FIGHT_DURATION];
	
	
}

-(void)stop_fight{
	
	if(_in_action==ACTION_FIGHTING){
		
		CGPoint center = [_image center];
		center.x -= (HERO_SWORD_WIDTH-HERO_WIDTH)/2;
		[_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
		[_image setCenter:center];
		[_image setAnimationImages:_run_array];
		[_image startAnimating];
		_in_action = ACTION_NORMAL;
	}
}

-(void)stop_restoring{
	
	
	
	CGPoint center = [_image center];
	[_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
	[_image setCenter:center];
	[_image setAnimationImages:_run_array];
	[_image startAnimating];
	
	if(_in_action==ACTION_RESTORING){

		_in_action = ACTION_NORMAL;
	}
	else if(_in_action==ACTION_RESTORING_JUMPING_UP){
		_in_action = ACTION_JUMPING_UP;
		[_image stopAnimating];
		[_image setImage:[UIImage imageNamed:@"hero_jump"]];
	}
	else if(_in_action==ACTION_RESTORING_JUMPING_DOWN){
		_in_action = ACTION_JUMPING_DOWN;
		[_image stopAnimating];
		[_image setImage:[UIImage imageNamed:@"hero_jump"]];
	}
	
	[_image setAlpha:1.0];
}


-(void)lost_life{
	
	_lives--;
	
	if(_lives<0){
		NSLog(@"You are dead! Try it again.");
		
	}
}


-(void)take_a_horse{
	
	if(!_at_horse){
		
		[_game_controller play_sound:SOUND_HORSE];
		
		_at_horse = YES;
		
		CGPoint center = [_image center];
		center.y -= (HORSE_HEIGHT-HERO_HEIGHT)/2;
		[_image setFrame:CGRectMake(0,0,HORSE_WIDTH, HORSE_HEIGHT)];
		[_image setCenter:center];
		[_image setAnimationImages:_horse_array];
		[_image startAnimating];
		
		
		[self performSelector:@selector(fall_down_from_horse) withObject:nil afterDelay:HORSE_DURATION];
	}
	
}


-(void)fall_down_from_horse{
	
	
	CGPoint center = [_image center];
	center.y += (HORSE_HEIGHT-HERO_HEIGHT)/2;
	[_image setFrame:CGRectMake(0,0,HERO_WIDTH, HERO_HEIGHT)];
	[_image setCenter:center];
	[_image setAnimationImages:_run_array];
	[_image startAnimating];
	
	_at_horse = NO;
	
}

-(void)pause{
	
	//stopping animations
	
    CFTimeInterval pausedTime = [_image.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    _image.layer.speed = 0.0;
    _image.layer.timeOffset = pausedTime;
}


-(void)play{
	
    CFTimeInterval pausedTime = [_image.layer timeOffset];
    _image.layer.speed = 1.0;
    _image.layer.timeOffset = 0.0;
    _image.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [_image.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    _image.layer.beginTime = timeSincePause;
	
}


@end
