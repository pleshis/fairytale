//
//  MYRestoStore.h
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MYRestaurant.h"

@interface MYRestoStore : NSObject
{
	NSMutableArray *all_restos;
	NSManagedObjectContext *context;
	NSManagedObjectModel *model;
}

-(void)load_all_restaurants;
-(NSArray*)all_restos;
-(MYRestaurant*)create_resto;
-(NSString*)resto_archive_path;
-(BOOL)save_changes;


@end
