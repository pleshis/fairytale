//
//  MYmenu_controller.h
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MYstory_view;
@class MYcredits_view;

@interface MYmenu_controller : UIViewController

@property(nonatomic,weak) IBOutlet UIImageView *running_man;
@property(nonatomic,weak) IBOutlet UIImageView *princess;
@property(nonatomic,weak) IBOutlet UIImageView *horse_man;

@property(nonatomic,strong) MYstory_view *story_view;
@property(nonatomic,strong) MYcredits_view *credits_view;


@property (nonatomic) CGSize screen_resolution;
@property (nonatomic) int state;


-(IBAction)start_game:(id)sender;
-(IBAction)show_story:(id)sender;
-(IBAction)show_credits:(id)sender;

-(IBAction)close_story:(id)sender;
-(IBAction)close_credits:(id)sender;

@end


#define STATE_DEFAULT	0
#define STATE_STORY		1
#define STATE_CREDITS	2
