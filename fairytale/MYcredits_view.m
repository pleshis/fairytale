//
//  MYcredits_view.m
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYmenu_controller.h"
#import "MYcredits_view.h"
#import <QuartzCore/QuartzCore.h>

@implementation MYcredits_view


- (id)init_with_MYmenu_controller:(MYmenu_controller*)menu_controller{
	
	self = [[[NSBundle mainBundle] loadNibNamed:@"MYcredits_view" owner:menu_controller options:nil] objectAtIndex:0];
	
    if (!self) {
		return self;
    }
	
	[[self layer] setCornerRadius:50.0];
	
    return self;
}

@end
