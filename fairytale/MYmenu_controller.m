//
//  MYmenu_controller.m
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYAppDelegate.h"
#import "MYmenu_controller.h"
#import "MYstory_view.h"
#import "MYcredits_view.h"

@interface MYmenu_controller ()

@end

@implementation MYmenu_controller

- (id)init{
	
    self = [super initWithNibName:@"MYmenu_controller"	bundle:nil];
    if(!self) {
		return self;
    }
	
	
	_state = STATE_DEFAULT;
	
	
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	
	
	//screen resolution
	_screen_resolution = [[UIScreen mainScreen] bounds].size;
	_screen_resolution = CGSizeMake(MAX(_screen_resolution.width,_screen_resolution.height),
									MIN(_screen_resolution.width,_screen_resolution.height));
	
	
	//run configuration
	[_running_man setAnimationDuration:1.0/2];
	[_running_man setAnimationImages:[NSArray arrayWithObjects:
									  [UIImage imageNamed:@"hero_01"],
									  [UIImage imageNamed:@"hero_02"],
									  [UIImage imageNamed:@"hero_03"],nil]];
	[_running_man startAnimating];
	
	//princess configuration
	[_princess setAnimationDuration:1.0/2];
	[_princess setAnimationImages:[NSArray arrayWithObjects:
								   [UIImage imageNamed:@"princess_01"],
								   [UIImage imageNamed:@"princess_02"],nil]];
	[_princess startAnimating];
	
	//princess configuration
	[_horse_man setAnimationDuration:1.0/2];
	[_horse_man setAnimationImages:[NSArray arrayWithObjects:
									[UIImage imageNamed:@"horse_02"],
									[UIImage imageNamed:@"horse_01"],
									[UIImage imageNamed:@"horse_03"],
									[UIImage imageNamed:@"horse_01"],nil]];
	[_horse_man startAnimating];
	
	
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)start_game:(id)sender{
	
	if(_state!=STATE_DEFAULT){
		return;
	}
	
	[(MYAppDelegate*)[[UIApplication sharedApplication] delegate] start_new_game];
	
}


-(IBAction)show_story:(id)sender{
	
	
	if(_state!=STATE_DEFAULT){
		return;
	}
	
	_state = STATE_STORY;
	
	_story_view = [[MYstory_view alloc] init_with_MYmenu_controller:self];
	[self.view addSubview:_story_view];
	
	
	[_story_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5-ANIMATION_OFFSET)*_screen_resolution.height)];
	[_story_view setAlpha:0.0];
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_story_view setCenter:CGPointMake(_screen_resolution.width/2, _screen_resolution.height/2)];
						 [_story_view setAlpha:1.0];
                     }
					 completion:^(BOOL finished){
						 
					 }];
	
}


-(IBAction)show_credits:(id)sender{
	
	
	if(_state!=STATE_DEFAULT){
		return;
	}
	
	_state = STATE_CREDITS;
	
	_credits_view = [[MYcredits_view alloc] init_with_MYmenu_controller:self];
	[self.view addSubview:_credits_view];
	
	[_credits_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5-ANIMATION_OFFSET)*_screen_resolution.height)];
	[_credits_view setAlpha:0.0];
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_credits_view setCenter:CGPointMake(_screen_resolution.width/2, _screen_resolution.height/2)];
						 [_credits_view setAlpha:1.0];
                     }
					 completion:^(BOOL finished){
						 
					 }];
}


-(IBAction)close_story:(id)sender{
	
	
	_state = STATE_DEFAULT;
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_story_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5+ANIMATION_OFFSET)*_screen_resolution.height)];
						 [_story_view setAlpha:0.0];
                     }
					 completion:^(BOOL finished){
						 [_story_view removeFromSuperview];
					 }];
}

-(IBAction)close_credits:(id)sender{
	
	_state = STATE_DEFAULT;
	
	[UIView animateWithDuration:ANIMATION_TIME
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 [_credits_view setCenter:CGPointMake(_screen_resolution.width/2, (0.5+ANIMATION_OFFSET)*_screen_resolution.height)];
						 [_credits_view setAlpha:0.0];
                     }
					 completion:^(BOOL finished){
						 [_credits_view removeFromSuperview];
					 }];
	
}


@end
