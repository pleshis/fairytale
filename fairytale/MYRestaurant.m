//
//  MYRestaurant.m
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYRestaurant.h"


@implementation MYRestaurant

@dynamic name;
@dynamic price;
@dynamic rating;
@dynamic last_modified;


//was creating new object
-(void)awakeFromInsert{
	
	[super awakeFromInsert];
	
//	NSTimeInterval *now = [NSDate dateWithTimeIntervalSinceReferenceDate:nil];
	NSDate *now = [NSDate date];
	self.last_modified = now;
}


//new constructor (initializer)
-(void)awakeFromFetch{
	
	[super awakeFromFetch];
//	UIImage *image = [UIImage imageWithData:self.image_data];
//	[self setPrimitiveValue:image forKey:@"image"];
	
}


-(void)add_one_star{
//	self.rating = [NSNumber numberWithDouble:[self.rating doubleValue]+1.0];
	self.rating += 1.0;
}


@end
