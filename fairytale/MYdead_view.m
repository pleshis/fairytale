//
//  MYdead_view.m
//  fairytale
//
//  Created by Jan Plesek on 21.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYdead_view.h"
#import "MYgame_controller.h"
#import <QuartzCore/QuartzCore.h>

@implementation MYdead_view

- (id)init_with_MYgame_controller:(MYgame_controller*)game_controller{
	
	self = [[[NSBundle mainBundle] loadNibNamed:@"MYdead_view" owner:game_controller options:nil] objectAtIndex:0];
	
    if (!self) {
		return self;
    }
	
	
	[[self layer] setCornerRadius:50.0];
	
    return self;
}


@end
