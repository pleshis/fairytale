//
//  MYRestoStore.m
//  fairytale
//
//  Created by Jan Plesek on 19.03.13.
//  Copyright (c) 2013 Jan Plesek. All rights reserved.
//

#import "MYRestoStore.h"

@implementation MYRestoStore


-(id)init{
	
	if(!self){
		return self;
	}
	
	model = [NSManagedObjectModel mergedModelFromBundles:nil];
	
	
	//link between model and database on the disc (storage)
	NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
	NSURL *store_url = [NSURL fileURLWithPath:self.resto_archive_path];
	
	NSError *error = nil;
	if(![coordinator addPersistentStoreWithType:NSSQLiteStoreType
								  configuration:nil
											URL:store_url
										options:nil
										  error:&error]){
		
		[NSException raise:@"Could not load stored restaurants" format:@"Reason: %@",error.localizedDescription];
	}
	
	
	//creating context
	context = [[NSManagedObjectContext alloc] init];
	context.persistentStoreCoordinator = coordinator;
	context.undoManager = nil;
	
	[self load_all_restaurants];
	
	return self;
}

-(void)load_all_restaurants{
	
	if(!all_restos){
		
		NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"MYRestaurant"];
		NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];	//how to sort items in table
		request.sortDescriptors = @[descriptor];	//this is NSArray, i can have there more then one descriptor
		
		NSError *error = nil;
		NSArray *result = [context executeFetchRequest:request
												 error:&error];
		
		if(!result){
			[NSException raise:@"Fetch failed"
						format:@"Reason: %@",error.localizedDescription];
		}
		
//		all_restos = [[NSMutableArray alloc] initWithArray:result];
		all_restos = [result mutableCopy];
		
	}
}

-(NSArray*)all_restos{
	
	return all_restos;
}


-(NSString*)resto_archive_path{
	
	NSArray *document_directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
																		NSUserDomainMask,
																		YES);
	NSString *document_directory = document_directories[0];
	return [document_directory stringByAppendingFormat:@"restos.data"];
	
}

-(BOOL)save_changes{
	
	NSError *error = nil;
	BOOL success = [context save:&error];
	if(!success){
		[NSException raise:@"Save failed"
					format:@"Reason: %@",error.localizedDescription];
		
	}
	return success;
}

//here is place where i should add other values
-(MYRestaurant*)create_resto{
	
	MYRestaurant *resto = [NSEntityDescription insertNewObjectForEntityForName:@"MYRestaurant"
														inManagedObjectContext:context];
	[all_restos addObject:resto];
	return resto;
}


@end
